import sys
import os
import csv
import pandas
import scipy.io
from PyQt4 import QtGui, QtCore

IC_READY = 'images/ic_go_search_api_holo_light.png'
IC_FINISHED = 'images/ic_checkmark_holo_light.png'
IC_ERROR = 'images/ic_dialog_alert_holo_light.png'

def convert_file(csvpath):
    matpath = csvpath[:-4] + '.mat'

    # read first line of csv file to get column names and replace ' ' with '_'
    r = csv.reader(open(csvpath))
    header = r.next()
    names = [x.replace(' ', '_') for x in header]
    
    # load the data into a pandas dataframe using the appropriate names
    df = pandas.read_csv(csvpath, names=names, header=0, skiprows=1, skip_footer=1)
    
    # write the dataframe to a mat file 
    df_dict = {c: list(df[c]) for c in df.columns}
    scipy.io.savemat(matpath, df_dict, oned_as='column')


class ConvertThread(QtCore.QThread):
    def __init__(self, filelist):
        QtCore.QThread.__init__(self)
        self.filelist = filelist

    def run(self):
        for f in self.filelist:
            csvpath = f.filepath
            convert_file(csvpath)
            self.emit(QtCore.SIGNAL('update'), f)


class FileListItem(QtGui.QListWidgetItem):
    def __init__(self, *args, **kwargs):
        super(FileListItem, self).__init__(*args, **kwargs)

    def setFile(self, url):
        self.filepath = str(url.toLocalFile())
        self.fileext = os.path.splitext(self.filepath)[-1].lower()
        self.filename = os.path.basename(self.filepath)
        super(FileListItem, self).setText(self.filename)

class MainWindow(QtGui.QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        self.initUI()

    def initUI(self):
        self.setAcceptDrops(True)

        self.instructionsLabel = QtGui.QLabel('Drag and drop CSV files to convert:')

        self.progressBar = QtGui.QProgressBar(self)
        self.progressBar.setRange(0, 1)
        self.progressBar.setValue(1)
        self.progressBar.setTextVisible(False)

        self.listWidget = QtGui.QListWidget(self)
        self.listWidget.setSelectionMode(QtGui.QAbstractItemView.ExtendedSelection)

        self.okButton = QtGui.QPushButton('Convert')
        self.okButton.setDefault(True)
        self.okButton.setEnabled(False)
        self.okButton.clicked.connect(self.processFiles)
        self.cancelButton = QtGui.QPushButton('Clear List')
        self.cancelButton.clicked.connect(self.clearFileList)

        self.buttonRow = QtGui.QHBoxLayout()
        self.buttonRow.addStretch(1)
        self.buttonRow.addWidget(self.okButton)
        self.buttonRow.addWidget(self.cancelButton)

        self.mainLayout = QtGui.QVBoxLayout()
        self.mainLayout.addWidget(self.instructionsLabel)
        self.mainLayout.addWidget(self.listWidget)
        self.mainLayout.addWidget(self.progressBar)
        self.mainLayout.addLayout(self.buttonRow)
        self.setLayout(self.mainLayout)

        self.setWindowTitle('CSV to MAT Converter')
        self.setGeometry(300, 300, 600, 250)
        self.show()

    def processFiles(self):
        items = []
        for i in xrange(self.listWidget.count()):
            items.append(self.listWidget.item(i))

        self.convertThread = ConvertThread(items)
        self.connect(self.convertThread, QtCore.SIGNAL('update'), self.conversionFinished)
        self.convertThread.start()
        self.progressBar.setRange(0, 0)
    
    def clearFileList(self):
        self.listWidget.clear()
        self.okButton.setEnabled(False)

    def conversionFinished(self, item):
        item.setIcon(QtGui.QIcon(IC_FINISHED))
        self.progressBar.setRange(0, 1)
        self.progressBar.setValue(1)

    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Delete:
            for item in self.listWidget.selectedItems():
                self.listWidget.takeItem(self.listWidget.row(item))

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls:
            e.accept()
        else:
            e.ignore()

    def dragMoveEvent(self, e):
        if e.mimeData().hasUrls:
            e.setDropAction(QtCore.Qt.CopyAction)
            e.accept()
        else:
            e.ignore()

    def dropEvent(self, e):
        if e.mimeData().hasUrls:
            e.setDropAction(QtCore.Qt.CopyAction)
            e.accept()
            
            for url in e.mimeData().urls():
                urlstr = str(url.toLocalFile())

                if os.path.isfile(urlstr):
                    item = FileListItem()
                    item.setFile(url)

                    if item.fileext == '.csv' and not self.listWidget.findItems(item.filename, QtCore.Qt.MatchExactly):
                        item.setIcon(QtGui.QIcon(IC_READY))
                        self.listWidget.addItem(item)
                        self.okButton.setEnabled(True)
                


def main():
    if len(sys.argv) > 1:
        for f in sys.argv[1:]:
            print "Converting: ", f
            convert_file(f)
            print "Done"

    else:
        app = QtGui.QApplication([])
        mw = MainWindow()
        app.exec_()

if __name__ == '__main__':
    main()
