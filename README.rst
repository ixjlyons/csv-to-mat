======================================
csv-to-mat : CSV to MAT file converter
======================================

Description
===========
This is a small Python project with a PyQt4 GUI that accepts CSV files dragged 
and dropped from the user's computer and converts them to MAT files. If the 
script is run from the command line with additional arguments, it will not use
the GUI and will assume that all arguments are CSV file paths. Currently, files
are converted in place. 

Usage
=====
To run the program with the GUI, just run the script with no arguments:

    $ python csv-to-mat.py

To run the program without the GUI, supply additional arguments that correspond
to files on your sysem:

    $ python csv-to-mat.py ~/data/file1.csv ~/data/file2.csv

If a whole folder contains nothing but CSV files, you could do something like:

    $ python csv-to-mat.py ~/data/*


Dependencies
============
Here are the modules outside stdlib that are being used. Version numbers are
just the ones I have installed. I don't know what the actual version
requirements are, but these definitely work:

* `PyQt4`_ (4.9.3) : the GUI toolkit of choice here
* `pandas`_ (0.8.0) : efficiently reads and parses big CSV files
* `scipy`_ (0.10.0) : scipy.io module can read/write mat files

A zip package of Windows installers for all dependencies in addition to Python
2.7 itself for 64-bit systems can be found in the Downloads section of this
Bitbucket repository. The project itself can also be packaged so users can run
the application without having Python installed at all.

TODO
====
* Add support for dragging and dropping folders.
* Better support for checking file types. CSV files need not have the .csv file
  extension...
* Implement some kind of error checking during the conversion process.
* Add a QFileDialog to specify a destination folder for the converted files.

.. DEPENDENCY LINKS
.. _`PyQt4`: http://www.riverbankcomputing.com/software/pyqt/intro
.. _`pandas`: http://pandas.pydata.org/
.. _`scipy`: http://www.scipy.org/
.. _`PyInstaller`: http://www.pyinstaller.org/
